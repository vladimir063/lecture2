package org.sber;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class Transfer {

    private Long id = 0L;
    private long transferCounter = 0L;
    private Double total;
    private String currency;
    private Receiver receiver;
    private Sender  sender;
    private LocalDateTime date = LocalDateTime.now();

    public Transfer(Double total, String currency, Receiver receiver, Sender sender) {
        this .id = transferCounter++;
        this.total = total;
        this.currency = currency;
        this.receiver = receiver;
        this.sender = sender;
    }

    public void doTransfer() {
        if (getSender().getAmountOfMoney() > getTotal()) {
            printResult();
        } else{
            System.out.println("Перевод совершить нельзя, недостаточно средств");
        }
    }

    public abstract void printResult();

    public Long getId() {
        return id;
    }

    public Double getTotal() {
        return total;
    }

    public String getCurrency() {
        return currency;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public Sender getSender() {
        return sender;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return Objects.equals(id, transfer.id) &&
                Objects.equals(total, transfer.total) &&
                Objects.equals(currency, transfer.currency) &&
                Objects.equals(receiver, transfer.receiver) &&
                Objects.equals(sender, transfer.sender) &&
                Objects.equals(date, transfer.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, total, currency, receiver, sender, date);
    }
}

