package org.sber;


import java.time.LocalDateTime;

public class TransferFromAccountToAccount extends Transfer{

    public TransferFromAccountToAccount(Double total, String currency, Receiver receiver, Sender sender) {
        super(total, currency, receiver, sender);
    }

    @Override
    public void printResult() {
        System.out.printf("ПЕРЕВОД СО СЧЕТА НА СЧЕТ: Номер перевода: %d, Дата: %s, Номер счета получателя: %s, Номер счета отправителя: %s," +
                        "Валюта: %s, Сумма: %f, Получатель: %s %s %s, Отправитель: %s %s %s.",
                getId(), getDate(), getReceiver().getAccountNumber(), getSender().getAccountNumber(), getCurrency(), getTotal(),
                getReceiver().getLastName(), getReceiver().getFirsName(), getReceiver().getPatronymic(),
                getSender().getLastName(), getSender().getFirsName(), getSender().getPatronymic());
    }
}
