package org.sber;

import java.time.LocalDateTime;

public class TransferFromCardToCard extends Transfer{

    public TransferFromCardToCard(Double total, String currency, Receiver receiver, Sender sender) {
        super(total, currency, receiver, sender);
    }

    @Override
    public void printResult() {
        System.out.printf("ПЕРЕВОД С КАРТЫ НА КАРТУ: Номер перевода: %d, Дата: %s, Номер карты получателя: %s, Номер карты отправителя: %s," +
                        "Валюта: %s, Сумма: %f, Получатель: %s %s %s, Отправитель: %s %s %s.",
                getId(), getDate(), getReceiver().getCardNumber(), getSender().getCardNumber(), getCurrency(), getTotal(),
                getReceiver().getLastName(), getReceiver().getFirsName(), getReceiver().getPatronymic(),
                getSender().getLastName(), getSender().getFirsName(), getSender().getPatronymic());
    }


}

