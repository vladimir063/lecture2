package org.sber;

import java.util.Objects;

public abstract class User {

    private String firsName;
    private String lastName;
    private String patronymic;
    private String cardNumber;
    private String accountNumber;
    private Double amountOfMoney = 0d;

    public String getFullName() {
        return String.format("%s %s %s", firsName, lastName, patronymic);
    }

    public String getFullNameToUppercase() {
        return getFullName().toUpperCase();
    }

    public String getFullNameToLowercase() {
        return getFullName().toLowerCase();
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public String getFirsName() {
        return firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firsName, user.firsName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(patronymic, user.patronymic) &&
                Objects.equals(cardNumber, user.cardNumber) &&
                Objects.equals(accountNumber, user.accountNumber) &&
                Objects.equals(amountOfMoney, user.amountOfMoney);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firsName, lastName, patronymic, cardNumber, accountNumber, amountOfMoney);
    }
}
